package com.filmsbykris.syscmd;

import java.util.*;
import java.net.*;
import java.io.*;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {

  TextView tv;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    tv=(TextView)findViewById(R.id.cmdOp);
    tv.setText("Output :"+"\n"+shell_exec("ls -la /sdcard/"));
  }

  public String shell_exec(String cmd)
  {
    String o=null;
    try
    {
      Process p=Runtime.getRuntime().exec(cmd);
      BufferedReader b=new BufferedReader(new InputStreamReader(p.getInputStream()));
      String line;
      while((line=b.readLine())!=null)o+=line;
    }catch(Exception e){o="error";}
    return o;
  }

}
